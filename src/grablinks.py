'''
Created on 09.07.2016

@author: chris
'''
import datetime
import os
from pprint import pprint
import re
from sys import argv


def grab_links(text):
    pattern = re.compile('http://goo.gl/[A-Za-z0-9]{6}')
    match = re.findall(pattern, text)
    return match


if __name__ == '__main__':
    if len(argv) == 2:
        filename = os.path.expanduser(argv[1])
        with open(filename, 'r') as myfile:
            data = myfile.read().replace('\n', '')
            links = grab_links(data)
    else:
        pprint('Usage: {} filename'.format(argv[0]))

    now = datetime.datetime.now()
    html = '''<html>
<head>
    <title>NHR-Newsticker</title>
</head>
<body>
<h1>NHR-Newsticker</h1>
<p>Erzeugt: {}</p>
<ul>
'''.format(now.strftime("%Y-%m-%d %H:%M"))

    i = 1
    for link in links:
        code = link.split('/')[-1]
        line = '<li><a href="https://goo.gl/#analytics/goo.gl/{}/all_time" target="_blank">News {}</a></li>'.format(
            code, i)
        i += 1

        html += '\t{}\n'.format(line)

    html += '''</ul>
</body>
</html>
'''
    with open(os.path.expanduser('~/Nordhessen-Rundschau/WhatsApp/nhr-newsticker.html'), 'w') as outfile:
        outfile.write(html)
