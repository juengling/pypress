'''
Created on 13.06.2015

@author: chris
'''
from datetime import date
from html2text import html2text
import logging
import os
import re


class WordpressInstallation(object):
    '''
    Contains all settings for a particular Wordpress installation

    :param name: Name of the Wordpress Installation (comes from the INI file)
    :param url:  URL of the Wordpress Installation (comes from the INI file)
    :param username: Username to login
    :param password: Password to login
    :param author(id): Author's name with the given ID
    '''

    def __init__(self):
        self._logger = logging.getLogger('WordpressInstallation')
        self._name = ''
        self._url = ''
        self._username = ''
        self._password = ''
        self._articles = []
        self._authors = {}

    def addArticle(self, article):
        '''
        Add information on an article

        :param article: Article (WordpressArticle instance)
        '''
        self._articles.append(article)
        self._logger.debug('Article added: {0} ({1}) {2}'.format(
            article.id, article.author, article.title))

    @property
    def name(self):
        '''
        Name of the Wordpress website (comes from the Settings file)
        '''
        return(self._name)

    @name.setter
    def name(self, newvalue):
        self._name = newvalue

    @property
    def url(self):
        '''
        URL of the Wordpress website (comes from the Settings file)
        '''
        return(self._url)

    @url.setter
    def url(self, newvalue):
        self._url = newvalue

    @property
    def username(self):
        '''
        Username necessary to connect to the Wordpress website (comes from the Settings file)
        '''
        return(self._username)

    @username.setter
    def username(self, newvalue):
        self._username = newvalue

    @property
    def password(self):
        '''
        Password necessary to connect to the Wordpress website (comes from the Settings file)
        '''
        return(self._password)

    @password.setter
    def password(self, newvalue):
        self._password = newvalue

    def get_author(self, author_id):
        if author_id in self._authors:
            return self._authors[author_id]
        else:
            raise KeyError(
                'Author with author_id {} not found.'.format(author_id))

    def set_author(self, author_id, name):
        self._authors[author_id] = name

    @property
    def articles(self):
        '''
        Return the list of articles
        '''
        return self._articles


class WordpressArticle(object):
    '''
    Contains the content for a particular Wordpress article
    '''

    def __init__(self):
        self._logger = logging.getLogger('WordpressArticle')
        self._id = ''
        self._author = ''
        self._authors_by_signature = ''
        self._photographers_by_signature = ''
        self._proofreaders_by_signature = ''
        self._title = ''
        self._date = None
        self._body_html = ''
        self._body_text = ''
        self._body_markdown = ''
        self._body_wordcount = 0
        self._body_imagecount = 0

    def __str__(self):
        return '{0}: {1} ({2})'.format(self._id, self._title, self._author)

    def write_to_file(self, filename):
        '''
        Write this article information to a file.

        There are two files used:

            * <id>.html for the content
            * <id>.json for the meta information

        :param filename: The file name including path, but without extension
        '''

        file = os.path.expanduser(os.path.expanduser(filename))
        self._logger.debug('Write article {0} to file {1}'.format(self, file))

        os.makedirs(os.path.dirname(file), exist_ok=True)

        # metadata = {'id': self._id,
        #            'author': self._author,
        #            'title': self._title}

#        with open(file + '.json', "w") as f:
#            f.write(json.dumps(metadata))

        with open(file + '.html', "w") as f:
            f.write(self._body_html)

        with open(file + '.md', "w") as f:
            f.write(self._body_markdown)

    def read_from_file(self, filename):
        '''
        Read this article from a file.

        :param filename: The file name including path
        '''
        raise NotImplementedError

    @property
    def id(self):
        return(self._id)

    @id.setter
    def id(self, newvalue):
        self._id = int(newvalue)

    @property
    def author(self):
        return(self._authors_by_signature or self._author)

    @author.setter
    def author(self, newvalue):
        self._author = newvalue

    @property
    def photographer(self):
        return(self._photographers_by_signature)

    @property
    def proofreader(self):
        return(self._proofreaders_by_signature)

    @property
    def title(self):
        return self._title
        # return '{} ({})'.format(self._title, self._id)

    @title.setter
    def title(self, newvalue):
        newvalue = newvalue.strip()
        if len(newvalue) > 0:
            self._title = newvalue
        else:
            self._title = '(no content)'

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, newvalue):
        if isinstance(newvalue, date):
            self._date = newvalue
        else:
            raise TypeError('%s is not a valid date'.format(newvalue))

    @property
    def body_html(self):
        return self._body_html

    @body_html.setter
    def body_html(self, newvalue):
        newvalue = newvalue.strip()
        if len(newvalue) > 0:
            self._body_html = newvalue
        else:
            self._body_html = '(no content)'

        self._body_markdown = html2text(self._body_html)
        self._body_text = self._body_markdown

        self._body_wordcount = len(self._body_markdown.split())
        self._body_imagecount = self._body_html.count('<img ')

        authors, photographers, proofreaders = detect_signature(
            self._body_text)

        self._authors_by_signature = get_real_name(authors)

        # Assume photographers == authors, if there no photographer found
        if len(photographers) == 0:
            self._photographers_by_signature = self._authors_by_signature
        else:
            self._photographers_by_signature = get_real_name(photographers)

        if len(proofreaders) > 0:
            self._proofreaders_by_signature = get_real_name(proofreaders)

    @property
    def body_text(self):
        return self._body_text

    @property
    def body_wordcount(self):
        return self._body_wordcount

    @property
    def body_imagecount(self):
        return self._body_imagecount

    @property
    def body_markdown(self):
        return self._markdown_text

    @property
    def is_press_release(self):
        return self._authors_by_signature and self._author == 'NHR'


def detect_signature(text):
    '''
    Detect author's and proofreader's signatures at the end of the article text
    and return it (or empty listm, if nothing could be found).
    '''

    text_authors = []
    pic_authors = []
    proofreaders = []
    text_author_found = False

    # Find all matches like (AB), (AB-CD), (AB-cd), (AB-CD-ef), ...
    for match in re.finditer(r'\([A-Za-z\-]+\)', text):
        for author in match.group(0).split('-'):
            author = author.strip('()')
            realname = get_real_name(author)
            if realname:
                if get_author_state(author) == 'author':
                    # Consider the first author as text author,
                    # any other as photographer
                    if text_author_found:
                        pic_authors.append(author.upper())
                    else:
                        text_authors.append(author.upper())
                        text_author_found = True
                elif get_author_state(author) == 'proofreader':
                    proofreaders.append(author.upper())

    return text_authors, pic_authors, proofreaders


def get_real_name(sig):
    logger = logging.getLogger('get_real_name')

    authors = {
        'AE': 'Armin Engelhardt',
        'AS': 'Alex Stein',
        'AU': 'Alexandra Uhlig',
        'CB': 'Christian Bültemann',
        'CJ': 'Christoph Jüngling',
        'CK': 'Christina Klimt',
        'IB': 'Ines Bültemann',
        'JB': 'Jörg Brezina',
        'JH': 'Heiko Jakob',
        'HJ': 'Heiko Jakob',
        'KH': 'Karl-Heinz Jacob',
        'LM': 'Lutz Müller',
        'MK': 'Manfred Kittner',
        'RL': 'Reimund Lill',
        'SF': 'Sehfahrer',
        'WS': 'Wolfgang Schuster',
        'YS': 'Yasmin Schwarze'
    }

    result = []

    if type(sig) is str:
        sig = [sig]

    for s in sig:
        try:
            result.append(authors[s.upper()])
        except KeyError:
            logger.error('Unknown author code: {}'.format(sig))

    return ', '.join(result)


def get_author_state(sig):
    if sig == sig.upper():
        return 'author'
    elif sig == sig.lower():
        return 'proofreader'
    else:
        return ''
