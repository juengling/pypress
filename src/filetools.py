# coding: utf-8

'''
Created on 15.06.2015

@author: chris
'''

import os
import platform


def getActualFilePathForRead(pgmname, filename, filetype):
    '''
    Create full path to the given file to be read, assuming it does exist,
    dependent on OS and intended file type.

    Example:

    A call of ``getActualFilePathForRead('myprogram', 'errorlog', 'log')`` will find a file in

      * on Linux:   './errorlog.log' or '/var/log/errorlog.log'
      * on Windows: C:\\Users\\myusername\\AppData\\Local\\errorlog.log or ...LocalLow... or ...Roaming...

    :param filename:   File name
    :param filetype:   Intended file type (i.e. extension)
    '''

    p = ['.']
    f = getFullFilename(filename, filetype)

    if platform.system() == 'Linux':
        if filetype == 'ini':
            p = ['.', '~']
            f = '.' + filename
        elif filetype == 'log':
            p = ['.', '/var/log/']
    elif platform.system() == 'Windows':
        p = ['.',
             'C:\\Users\\{0:s}\\AppData\\Local\\{1:s}\\'.format(
                 os.getlogin(), pgmname),
             'C:\\Users\\{0:s}\\AppData\\LocalLow\\{1:s}\\'.format(
                 os.getlogin(), pgmname),
             'C:\\Users\\{0:s}\\AppData\\Roaming\\{1:s}\\'.format(
                 os.getlogin(), pgmname)
             ]

    # Look for first existing file. If nothing found, return empty string
    actual_file = ''
    for actual_path in p:
        actual_file = os.path.join(actual_path, f)
        if os.path.exists(actual_file):
            break

    return(actual_file)


def getFullFilename(filename, filetype):
    '''
    Make full file name from file name and type

    :param filename: File name (without '.')
    :param filetype: File type (without '.')
    '''
    if filetype:
        return(filename + '.' + filetype)
    else:
        return(filename)
