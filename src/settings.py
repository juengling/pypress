# coding: utf-8

'''
Created on 13.06.2015

@author: chris
'''

import configparser
import json
import logging
from pprint import pprint

from wordpressinstallation import WordpressInstallation


class Settings(object):
    '''
    The Settings class holds and provides (read-only) any setting the program may need
    '''

    def __init__(self, inifilecontents):
        self._logger = logging.getLogger('Settings')
        self._wpilist = list()
        self._config = configparser.RawConfigParser()
        self._config.read_string(inifilecontents)
        self._localfolder = ''

        self._readsettings()

    def _readsettings(self):
        '''
        Read the settings from the ressource
        '''
        self._logger.debug('Settings found:')
        for s in self._config.sections():
            if s.lower() == 'options':
                self._localfolder = self._config[s]['localfolder']
                self._logger.debug(
                    '   - Local folder: {}'.format(self._localfolder))
            elif s.startswith('WP:'):
                wpi = WordpressInstallation()
                wpi.name = s[3:]
                wpi.url = self._config[s]['url']
                wpi.username = self._config[s]['user']
                wpi.password = self._config[s]['pwd']

                authors = self._readauthors(self._config[s]['authors'])
                for author in authors:
                    wpi.set_author(author[0], author[1])

                self._wpilist.append(wpi)
                self._logger.debug(
                    '   - {0} ({1}@{2})'.format(s, wpi.username, wpi.url))

    def _readauthors(self, section):
        self._logger.debug('Trying to read authors from config file ...')
        authors = self._config.items(section)
        self._logger.info('%i authors found', len(authors))
        return authors

    @property
    def wordpressinstallations(self):
        '''
        Return a list of WordPress installations found in the settings file
        '''
        return(self._wpilist)

    @property
    def folder(self):
        '''
        Return the local folder, where the articles shall be stored
        '''
        return self._localfolder
