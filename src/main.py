'''
Preconditions:

  - Python 3
  - pip install python-wordpress-xmlrpc


'''


from argparse import ArgumentParser
import logging
from os import path, getcwd, getlogin
import platform
import sys
from time import time
from wordpress_xmlrpc import Client
from wordpress_xmlrpc.exceptions import InvalidCredentialsError
from wordpress_xmlrpc.methods.posts import GetPosts
from xmlrpc.client import Fault

from filetools import getActualFilePathForRead
from settings import Settings
from wordpressinstallation import WordpressArticle


__myname__ = 'PyPress'
__myshortname__ = 'pypress'
__title__ = 'Python-Wordpress-Up-/Downloader'
__version__ = '0.1.0'


verbose = 0


def main():
    '''
    PyPress is (intended to be) a WordPress up- and downloader.
    The goal is, if a connection to the WordPress instance is
    possible, to (1) download all posts (or any other selection
    that may be possible in the future) and store them into local
    files, and (2) provide upload possibilities for a single article
    (or any other selection that may be possible in the future).
    '''

    global verbose

    args = parse_command_line()

    if args.version:
        print('{0:s} v{1:s}\n{2:s}'.format(__myname__, __version__, __title__))
        return(0)

    # Init logging
    if args.verbose:
        verbose = args.verbose
    else:
        verbose = 0

    if args.log:
        if args.logfile:
            # Use the given log file
            logging.basicConfig(filename=fullpath(args.logfile),
                                format='%(asctime)s; %(levelname)s; %(name)s; %(message)s',
                                level=logging.INFO)
        else:
            # Use default log file
            logging.basicConfig(filename=getlogfilename(),
                                format='%(asctime)s; %(levelname)s; %(name)s; %(message)s',
                                level=logging.INFO)

    else:
        logging.basicConfig(level=logging.ERROR)

    logger = logging.getLogger(__myname__)

    try:
        logger.debug('Start program')

        settings = read_settings(__myshortname__, logger)
        # if not settings: settings = read_settings(fullpath(args.ini), logger)
        # if not settings: settings = read_settings(getlocalsettingsfilepath(args.ini), logger)
        # if not settings: settings = {}
    except Exception:
        logger.exception('Error')
        return 2

    noaction = True

    if args.downloadarticles:
        noaction = False
        download_articles(
            settings.wordpressinstallations, settings.folder, args.storearticles)
    elif args.storearticles:
        print('Storing articles without downloading them is not possible!')

    if noaction:
        logger.info('No action performed. See command "--help" for help.')
    logger.debug('Finished.')

    # wp.call(GetUserInfo())


def parse_command_line():
    '''
    Parse the command line parameters
    '''
    parser = ArgumentParser(
        prog=__title__ + ' v' + __version__, description=main.__doc__)

    parser.add_argument('--downloadarticles', action='store_true',
                        help='Read articles from remote')
    parser.add_argument('--storearticles', action='store_true',
                        help='Store articles into local files (even if not specified, a .csv file is saved though)')
    parser.add_argument('--version', action='store_true',
                        help='Print version information and exit')
    parser.add_argument('-v', '--verbose', action='count',
                        help='Enhance printed details')
    parser.add_argument('-l', '--log', action='store_true',
                        help='Activate logging to file')
    parser.add_argument('--logfile',
                        help='Name or path for the logfile'
                        ' (defaults to pypress.log in te os\'s/user\'s default log directory)')

    args = parser.parse_args()
    return(args)


def login(wpi):
    '''
    Login to the website.
    The user must have at least editor privileges,
    otherwise he will only see his own articles.

    :param wpi: Wordpress installation object
    '''
    logger = logging.getLogger('login')
    logger.debug('Login to %s with user %s', wpi.name, wpi.username)
    try:
        rpc_url = 'http://{0}/xmlrpc.php'.format(wpi.url)
        result = Client(rpc_url, wpi.username, wpi.password)
    except TimeoutError:
        logger.error('Timeout during login')
        result = None

    return result


def getpost(serverinstance, offset=0, maximum=0):
    '''
    Generator to retrieve wordpress posts from the server and provide them one by one.

    :param serverinstance: Wordpress server instance to retrieve articles from
    :param offset:  Start with this offset (default: 0)
    :param maximum: Maximum number of articles to retrieve (default: 0 = retrieve all) 
    '''

    def getnewinc(factor):
        '''
        Handle bad experience by reducing the increment for the next loop

        :param factor: Factor
        '''
        inc = int(increment * factor)

        if factor < 1:
            # Bad experience
            logger.info('Increment decreased to {}.'.format(inc))
        else:
            # Good experience
            logger.info(
                'Increment increased to {} because of successfull loop.'.format(inc))

        return inc

    logger = logging.getLogger('getpost')

    # Factors to change increment in case of factor_good or factor_bad experience
    # during the last loop
    factor_good = 1.6
    factor_bad = 0.5

    increment = 100
    number_of_retrieved_articles = 0

    while maximum == 0 or number_of_retrieved_articles <= maximum:
        if increment < 1:
            logger.info(
                'Increment too small, skipping post and restarting with default increment.')
            increment = 100
            offset += 1

        try:
            logger.info(
                'Retrieving articles {} - {}'.format(offset, offset + increment - 1))
            post_options = {'number': increment,
                            'offset': offset,
                            'post_status': 'publish',
                            'orderby': 'id',
                            'order': 'DESC'}
            posts = serverinstance.call(GetPosts(post_options))

        except (Fault, InvalidCredentialsError):
            logger.error('Login failed')
            raise

        except TimeoutError:
            logger.error(
                'Timeout during retrieval of an article group (offset: {}, amount: {}'.format(offset, increment))
            increment = getnewinc(factor_bad)
            continue

        except Exception as e:
            logger.exception(e)
            logger.error(
                'Post retrieval failed: Offset {}, increment {}'.format(offset, increment))
            increment = getnewinc(factor_bad)
            continue

        if len(posts) == 0:
            break  # no more posts returned

        # Return retrieved articles one by one
        for post in posts:
            yield post
            number_of_retrieved_articles += 1

        # Add used increment to the current offset
        offset += increment

        # Increase increment because of successfull loop
        increment = getnewinc(factor_good)


def download_articles(wordpressinstallations, folder, store=False):
    '''
    Download and store articles

    :param wordpressinstallations: Wordpress installations object
    :param folder: Folder to be saved to
    '''
    logger = logging.getLogger('download_and_store_articles')
    logger.info('Reading authors ...')

    count_ok = 0
    count_err = 0

    # Open CSV file for importing the meta information into LibreOffice
    for wpi in wordpressinstallations:
        logger.info('Working with "%s" ...', wpi.name)
        print('Working with "{}" ...'.format(wpi.name))

        start_time = time()

        count = 0
        serverinstance = login(wpi)
        if serverinstance:
            csv_filename = path.expanduser(path.join(
                folder, wpi.name, 'articles.csv'))

            with open(csv_filename, 'w+') as csv:
                for post in getpost(serverinstance):
                    count += 1
                    if count % 1000 == 0:
                        sys.stdout.write('\r{} articles found'.format(count))

                    article = WordpressArticle()
                    article.id = post.id
                    article.title = post.title

                    try:
                        article.author = wpi.get_author(post.user)
                    except KeyError:
                        article.author = 'Unknown author id: {}'.format(
                            post.user)

                    article.date = post.date
                    article.body_html = post.content
                    filename = path.join(
                        folder, wpi.name, '{:05d}'.format(article.id))

                    if store:
                        article.write_to_file(filename)

                    if article.is_press_release:
                        articletype = 'P'
                    else:
                        articletype = 'A'

                    csv.write('{title}\t{author}\t{photographer}\t{proofreader}\t{date}\t{words}\t{photos}\t{otherphotos}\t{km}\t{type}\n'.format(
                        title=cleartext(article.title),
                        author=article.author,
                        photographer=article._photographers_by_signature,
                        proofreader=article.proofreader,
                        date=germandate(article.date),
                        words=article.body_wordcount,
                        photos=article.body_imagecount,
                        otherphotos=0,
                        km=0,
                        type=articletype))

                    # wpi.addArticle(article)

                end_time = time()
                minutes, seconds = divmod(end_time - start_time, 60)
                print('\n\nDone, retrieved {articles:d} articles from {server:s} in {m:d}\'{s:02d}" ({avg:d} articles/s).'.format(
                    articles=count,
                    server=wpi.name,
                    m=int(round(minutes)),
                    s=int(round(seconds)),
                    avg=int(round(count / (end_time - start_time)))))


def read_settings(inifilename, logger):
    try:
        i = getActualFilePathForRead(__myshortname__, inifilename, 'ini')
        i = path.expanduser(i)
        logger.debug('Reading settings from {0} ...'.format(i))
        f = open(i, 'r')
        settings = Settings(f.read())
        f.close()
    except FileNotFoundError:
        settings = None
        logger.debug('No settings found in {0}'.format(inifilename))

    return(settings)


def getlogfilename():
    '''
    Determine default log file location on several operating systems
    '''
    if platform.system() == 'Linux':
        return('/tmp/{0:s}.log'.format(__myname__))
    elif platform.system() == 'Windows':
        return('C:\\Users\\{0:s}\\AppData\\Local\\{1:s}.log'.format(getlogin(), __myname__))


def fullpath(filename):
    if (path.dirname(filename) == ''):
        # If no path exists, add current work directory
        f = path.join(getcwd(), filename)
    else:
        f = filename

    return path.expanduser(f)


def germandate(d):
    result = '{tag}.{monat}.{jahr}'.format(
        tag=d.day, monat=d.month, jahr=d.year)
    return result


def cleartext(text):
    '''
    Remove some special signs which would CSV format

    :param text: Original text
    :return:     Cleared text
    '''
    return text.replace('"', '').replace('\t', '')

if __name__ == '__main__':
    sys.exit(main())
