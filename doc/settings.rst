Settings
========

.. automodule:: settings
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
