.. PyPress documentation master file

Welcome to PyPress's documentation!
===================================

Author: Christoph Jüngling <chris@juengling-edv.de>

Home: https://bitbucket.org/juengling/pypress

PyPress is (intended to be) a WordPress up- and downloader.
The goal is, if a connection to the WordPress instance is
possible, to 

	* download all posts (or any other selection that may be possible in the future) and store them into local files
	* provide upload possibilities for a single article	(or any other selection that may be possible in the future).     

This is the technical documentation of the software itself. A user manual is currently not yet available. 

Software
========

.. toctree::
   
	filetools
	settings
	wordpressinstallation
	

