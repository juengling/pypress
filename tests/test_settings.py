'''
Created on 20.03.2016

@author: chris
'''
import unittest
from settings import Settings


class TestSettings(unittest.TestCase):

    def setUp(self):
        self.settings = Settings('''
            [Options]
            localfolder=~/pypress-files
            
            [WP:Testsite]
            url=mywebsite.de
            user=ItsMyName
            pwd=abcxyz
            authors=Testsite-Authors
            
            [Testsite-Authors]
            1 = Chris
            2 = Anni
            42 =
            ''')

    def testEmptySettings(self):
        settings = Settings('')
        self.assertEqual(len(settings.wordpressinstallations), 0)

    def test1Server1Author(self):
        self.assertEqual(len(self.settings.wordpressinstallations), 1)

        self.assertEqual(self.settings.folder, '~/pypress-files')

        wpi = self.settings.wordpressinstallations[0]

        self.assertEqual(wpi.name, 'Testsite')
        self.assertEqual(wpi.url, 'mywebsite.de')
        self.assertEqual(wpi.username, 'ItsMyName')
        self.assertEqual(wpi.get_author('1'), 'Chris')

    def test1ServerManyAuthors(self):
        self.assertEqual(len(self.settings.wordpressinstallations), 1)

        self.assertEqual(self.settings.folder, '~/pypress-files')

        wpi = self.settings.wordpressinstallations[0]

        self.assertEqual(wpi.name, 'Testsite')
        self.assertEqual(wpi.url, 'mywebsite.de')
        self.assertEqual(wpi.username, 'ItsMyName')
        self.assertEqual(wpi.get_author('1'), 'Chris')
        self.assertEqual(wpi.get_author('2'), 'Anni')
        self.assertEqual(wpi.get_author('42'), '')

        with self.assertRaises(KeyError):
            x = wpi.get_author('77')  # @UnusedVariable


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
