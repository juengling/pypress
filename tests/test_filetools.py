# coding: utf-8

'''
Created on 15.06.2015

@author: chris
'''
import unittest
from filetools import getFullFilename, getActualFilePathForRead


class Test_filetools(unittest.TestCase):

    def test_getActualFilePathForRead_ini(self):
        actual = getActualFilePathForRead('testprogram', 'testfile', 'ini')
        self.assertEqual(actual, '~/.testfile')

    def test_getActualFilePathForRead_log(self):
        actual = getActualFilePathForRead('testprogram', 'testfile', 'log')
        self.assertEqual(actual, '/var/log/testfile.log')

    def test_getActualFilePathForRead_other(self):
        actual = getActualFilePathForRead('testprogram', 'testfile', 'ext')
        self.assertEqual(actual, './testfile.ext')
    
    def test_getFullFilename_WithExt(self):
        actual = getFullFilename('testfile', 'ini')
        self.assertEqual(actual, 'testfile.ini')

    def test_getFullFilename_NoExt(self):
        actual = getFullFilename('testfile', '')
        self.assertEqual(actual, 'testfile')

if __name__ == "__main__":
    unittest.main()