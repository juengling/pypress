'''
Created on 19.06.2016

@author: chris
'''
import unittest
from wordpressinstallation import detect_signature, get_real_name


class Test(unittest.TestCase):

    def test_detect_signature_1author(self):
        text = '''
HonigBerlin (ots) - In 13 von 22 getesteten Honigen 

Pressekontakt:

PM: Corinna Hölzel, BUND-Pestizidexpertin  (JH)
        
        '''

        authors, picauthors, correctors = detect_signature(text)

        self.assertEqual(authors, ['JH'])
        self.assertEqual(picauthors, [])
        self.assertEqual(correctors, [])

    def test_detect_signature_2authors(self):
        text = '''
HonigBerlin (ots) - In 13 von 22 getesteten Honigen 

Pressekontakt:

PM: Corinna Hölzel, BUND-Pestizidexpertin  (JH-LM)
        
        '''

        authors, picauthors, correctors = detect_signature(text)

        self.assertEqual(authors, ['JH'])
        self.assertEqual(picauthors, ['LM'])
        self.assertEqual(correctors, [])

    def test_detect_signature_1author_1proof(self):
        text = '''
HonigBerlin (ots) - In 13 von 22 getesteten Honigen 

Pressekontakt:

PM: Corinna Hölzel, BUND-Pestizidexpertin  (JH-lm)
        
        '''

        authors, picauthors, correctors = detect_signature(text)

        self.assertEqual(authors, ['JH'])
        self.assertEqual(picauthors, [])
        self.assertEqual(correctors, ['LM'])

    def test_detect_signature_2authors_1proof(self):
        text = '''
HonigBerlin (ots) - In 13 von 22 getesteten Honigen 

Pressekontakt:

PM: Corinna Hölzel, BUND-Pestizidexpertin  (JH-LM-cj)
        
        '''

        authors, picauthors, correctors = detect_signature(text)

        self.assertEqual(authors, ['JH'])
        self.assertEqual(picauthors, ['LM'])
        self.assertEqual(correctors, ['CJ'])

    def test_detect_signature_2authors_2proofs(self):
        text = '''
HonigBerlin (ots) - In 13 von 22 getesteten Honigen 

Pressekontakt:

PM: Corinna Hölzel, BUND-Pestizidexpertin  (JH-LM-cj-ys)
        
        '''

        authors, picauthors, correctors = detect_signature(text)

        self.assertEqual(authors, ['JH'])
        self.assertEqual(picauthors, ['LM'])
        self.assertEqual(correctors, ['CJ', 'YS'])

    def test_get_real_name_existing(self):
        self.assertEqual(get_real_name('CJ'), 'Christoph Jüngling')

    def test_get_real_name_2existing(self):
        self.assertEqual(
            get_real_name(['CJ', 'YS']), 'Christoph Jüngling, Yasmin Schwarze')

    def test_get_real_name_nonexisting(self):
        self.assertEqual(get_real_name('XY'), '')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_detect_signature']
    unittest.main()
